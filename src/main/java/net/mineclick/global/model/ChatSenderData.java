package net.mineclick.global.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class ChatSenderData extends PlayerId {
    @Getter
    private Map<String, String> info = new HashMap<>();

    public static ChatSenderData from(PlayerId playerId) {
        ChatSenderData senderData = new ChatSenderData();
        senderData.setUuid(playerId.getUuid());
        senderData.setName(playerId.getName());
        senderData.setRank(playerId.getRank());

        return senderData;
    }

    public static ChatSenderData from(PlayerModel playerModel) {
        ChatSenderData senderData = from(((PlayerId) playerModel));
        senderData.info = playerModel.getChatHoverInfo();

        return senderData;
    }
}
