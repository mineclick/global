package net.mineclick.global.model;

import lombok.*;
import net.mineclick.global.type.Rank;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class PlayerId {
    @NonNull
    @EqualsAndHashCode.Include
    @Setter(AccessLevel.PROTECTED)
    private UUID uuid;

    private boolean online;
    private boolean banned;
    private String server;
    private String name = "";
    private Rank rank = Rank.DEFAULT;
    private boolean premiumForLife;
    private int ping;
    private String discordId;
    private String texture;
    private String signature;
    private Instant createdAt = Instant.now();
    private Instant updatedAt = Instant.now();
}
