package net.mineclick.global.model;

import lombok.Data;

import java.util.concurrent.atomic.AtomicBoolean;

@Data
public class GameSettings {
    private AtomicBoolean numericNotation = new AtomicBoolean(false);
    private AtomicBoolean joinMsgDisabled = new AtomicBoolean(false);
}
