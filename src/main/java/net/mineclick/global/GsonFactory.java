package net.mineclick.global;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import net.mineclick.global.util.BigNumber;
import net.mineclick.global.util.location.LocationParser;
import org.bukkit.Location;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GsonFactory implements TypeAdapterFactory {
    protected final Map<Class<?>, TypeAdapter<?>> typeAdapterMap = new HashMap<>();

    public GsonFactory() {
        typeAdapterMap.put(Location.class, new TypeAdapter<Location>() {
            @Override
            public void write(JsonWriter jsonWriter, Location location) throws IOException {
                jsonWriter.value(location == null ? "" : LocationParser.toString(location));
            }

            @Override
            public Location read(JsonReader jsonReader) throws IOException {
                String string = jsonReader.nextString();
                if (string == null || string.isEmpty())
                    return null;

                try {
                    return LocationParser.parse(string);
                } catch (Exception e) {
                    GlobalPlugin.i().getLogger().severe("Could not parse Location: " + string);
                    return null;
                }
            }
        });

        typeAdapterMap.put(BigNumber.class, new TypeAdapter<BigNumber>() {
            @Override
            public void write(JsonWriter jsonWriter, BigNumber bigNumber) throws IOException {
                jsonWriter.value(bigNumber == null ? "0" : bigNumber.toString());
            }

            @Override
            public BigNumber read(JsonReader jsonReader) throws IOException {
                String string = jsonReader.nextString();
                if (string == null || string.isEmpty())
                    return BigNumber.ZERO;

                try {
                    return new BigNumber(string);
                } catch (Exception e) {
                    GlobalPlugin.i().getLogger().severe("Could not parse BigNumber: " + string);
                    return BigNumber.ZERO;
                }
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        Map.Entry<Class<?>, TypeAdapter<?>> entry = typeAdapterMap.entrySet().stream()
                .filter(e -> e.getKey().isAssignableFrom(typeToken.getRawType()))
                .findFirst().orElse(null);

        if (entry == null) return null;

        return (TypeAdapter<T>) entry.getValue();
    }
}
