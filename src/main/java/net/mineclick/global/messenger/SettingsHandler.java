package net.mineclick.global.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.global.model.ConfigurationsModel;
import net.mineclick.global.service.ConfigurationsService;

@Setter
@MessageName("settings")
public class SettingsHandler extends Message {
    @Getter
    private ConfigurationsModel configurations;

    @Override
    public void onReceive() {
        if (getAction().equals(Action.UPDATE)) {
            if (configurations != null && configurations.getConfigurations() != null) {
                ConfigurationsService.i().update(configurations.getConfigurations());
            }
        }
    }
}
