package net.mineclick.global.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.global.util.Strings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Compared to {@link ChatHandler} this is used to send a direct message to a specific user
 */
@Getter
@Setter
@MessageName("message")
public class MessageHandler extends Message {
    private String uuid;
    private String message;

    @Override
    public void onReceive() {
        if (!getAction().equals(Action.POST)) return;

        Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        if (player != null) {
            player.sendMessage(Strings.line());
            player.sendMessage(ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', message));
            player.sendMessage(Strings.line());

            send(Response.OK);
        }
    }
}
