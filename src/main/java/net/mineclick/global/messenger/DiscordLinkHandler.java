package net.mineclick.global.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.global.service.PlayersService;
import net.mineclick.global.util.MessageType;

import java.util.UUID;

@Getter
@Setter
@MessageName("discordLink")
public class DiscordLinkHandler extends Message {
    private String uuid;
    private String discordId;

    @Override
    public void onReceive() {
        if (getAction().equals(Action.DELETE) && discordId != null) {
            PlayersService.i().forAll(playerModel -> {
                if (playerModel.getDiscordId().equals(discordId)) {
                    playerModel.setDiscordId(null);
                    playerModel.sendMessage("Your Discord account has been unlinked", MessageType.ERROR);
                }
            });
            return;
        }

        if (!getAction().equals(Action.POST) || uuid == null || discordId == null) return;

        UUID uuid = UUID.fromString(this.uuid);
        PlayersService.i().get(uuid, playerModel -> {
            playerModel.setDiscordId(discordId);
            playerModel.setDiscordState(null);
            playerModel.sendImportantMessage("Success!", "You Discord account has been linked");
        });
    }
}