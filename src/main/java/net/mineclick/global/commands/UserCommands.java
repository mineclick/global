package net.mineclick.global.commands;

import com.google.common.collect.ImmutableMap;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.mineclick.global.service.ChatService;
import net.mineclick.global.service.OffencesService;
import net.mineclick.global.type.PunishmentType;
import net.mineclick.global.type.Rank;
import net.mineclick.global.util.Strings;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Map;

public class UserCommands {
    public static void load() {
        Commands.addCommand(Commands.Command.builder()
                .name("shrug")
                .callFunction((playerModel, strings) -> {
                    ChatService.i().sendPublicChat(playerModel, StringUtils.join(strings) + " ¯\\_(ツ)_/¯");
                    return null;
                })
                .hidden(true)
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("website")
                .callFunction((playerModel, strings) -> {
                    Strings.sendWebsite(playerModel.getPlayer());
                    return null;
                })
                .hidden(true)
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("store")
                .description("Link to the store page")
                .callFunction((playerModel, strings) -> {
                    Strings.sendStore(playerModel.getPlayer());
                    return null;
                })
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("wiki")
                .description("Link to the wiki page")
                .callFunction((playerModel, strings) -> {
                    Strings.sendWiki(playerModel.getPlayer());
                    return null;
                })
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("pl")
                .callFunction((playerModel, strings) -> ChatColor.YELLOW + "Couldn't find plugins because " + Strings.getFunnyReason()
                        + "\n" + ChatColor.GRAY + "In all seriousness, huge thanks to Spigot and BungeeCord communities!")
                .hidden(true)
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("joinmsg")
                .callFunction((playerModel, strings) -> {
                    if (!playerModel.getRank().isAtLeast(Rank.PAID))
                        return null;

                    boolean disabled = playerModel.getGameSettings().getJoinMsgDisabled().get();
                    playerModel.getGameSettings().getJoinMsgDisabled().set(!disabled);
                    return ChatColor.YELLOW + (disabled ? "Enabled" : "Disabled") + " join announcements";
                })
                .hidden(true)
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("rules")
                .description("Get a link to the rules page")
                .callFunction((playerModel, strings) -> {
                    Strings.sendRules(playerModel.getPlayer());
                    return null;
                })
                .build());

        Map<String, String> sites = ImmutableMap.of(
                "minecraftservers.com", "https://minecraftservers.org/vote/479656",
//                "minecraft-server-list.com", "http://minecraft-server-list.com/",
                "minecraft-mp.com", "https://minecraft-mp.com/server/185615/vote/"
        );
        Commands.addCommand(Commands.Command.builder()
                .name("vote")
                .description("A list of websites where you can vote")
                .callFunction((playerModel, strings) -> {
                    playerModel.getPlayer().sendMessage(Strings.line());
                    playerModel.getPlayer().sendMessage(Strings.middle(ChatColor.YELLOW + "Click to vote and get " + ChatColor.GREEN + "+" + ChatColor.AQUA + "100" + ChatColor.YELLOW + " schmepls and an hour of gold income"));

                    for (Map.Entry<String, String> e : sites.entrySet()) {
                        TextComponent text = new TextComponent(e.getKey());
                        text.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                        text.setItalic(true);
                        text.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, e.getValue()));
                        text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click!").color(net.md_5.bungee.api.ChatColor.DARK_AQUA).create()));
                        playerModel.getPlayer().spigot().sendMessage(text);
                    }

                    playerModel.getPlayer().sendMessage(Strings.line());

                    return null;
                })
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("msg")
                .description("Send a private message to a player")
                .usage("<playerName> <msg>")
                .minArgs(2)
                .playersTabComplete(true)
                .callFunction((playerModel, strings) -> {
                    if (OffencesService.i().isCurrentlyPunishedWith(playerModel, PunishmentType.MUTE)) {
                        ChatService.i().sendMutedMessage(playerModel);
                        return null;
                    }

                    String to = strings[0];
                    if (to.equalsIgnoreCase(playerModel.getName())) {
                        return ChatColor.RED + "Couldn't send the msg to " + to + " because " + Strings.getFunnyReason();
                    }

                    String msg = StringUtils.join(strings, " ").substring(to.length() + 1);
                    ChatService.i().sendPrivateChat(playerModel, to, msg);
                    return null;
                })
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("discord")
                .description("Get the Discord invite link")
                .callFunction((playerModel, strings) -> {
                    Strings.sendDiscord(playerModel.getPlayer());
                    return null;
                })
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("help")
                .description("See the list of commands")
                .hidden(true)
                .callFunction((playerModel, strings) -> {
                    Player player = playerModel.getPlayer();

                    player.sendMessage(Strings.line() + "\n" + Strings.middle(ChatColor.YELLOW + "MineClick Commands") + "\n \n");
                    for (Commands.Command command : Commands.commands) {
                        Rank rank = playerModel.getRank();
                        if (command.isHidden() || !rank.isAtLeast(command.getMinRank())) {
                            continue;
                        }

                        String text = (command.getMinRank().isAtLeast(Rank.STAFF) ? ChatColor.RED : ChatColor.DARK_GREEN)
                                + "/" + command.getName() + " " + ChatColor.GRAY + command.getDescription();
                        TextComponent textComponent = new TextComponent(TextComponent.fromLegacyText(text));

                        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(ChatColor.DARK_AQUA + "Click to execute /" + command.getName())));
                        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + command.getName()));

                        player.spigot().sendMessage(textComponent);
                    }
                    player.sendMessage("\n " + Strings.middle(ChatColor.YELLOW + "Website: " + ChatColor.DARK_AQUA + "mineclick.net") + "\n" + Strings.line());
                    return null;
                })
                .build());
    }
}
