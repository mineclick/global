package net.mineclick.global;

public class Constants {
    public static final boolean PROD = Boolean.parseBoolean(System.getenv("PROD"));
    public static final String REDIS_HOST = PROD ? "redis" : "localhost";
    public static final int REDIS_PORT = 6379;
    public static final String REDIS_PASSWORD = null;
    public static final String PREMIUM_STORE_ID = "MC-M001";
    public static final boolean QUICK_LOAD = Boolean.parseBoolean(System.getenv("QUICK_LOAD"));
    public static final String SENTRY_URL = System.getenv("SENTRY_URL");
}
