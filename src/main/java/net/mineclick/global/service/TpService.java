package net.mineclick.global.service;

import net.mineclick.core.messenger.Action;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.messenger.TpHandler;
import net.mineclick.global.model.PlayerId;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.util.MessageType;
import net.mineclick.global.util.SingletonInit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

@SingletonInit
public class TpService {
    private static TpService i;

    public static TpService i() {
        return i == null ? i = new TpService() : i;
    }

    /**
     * Tp to another players whether they are on the same server or not
     *
     * @param playerModel  The player to teleport
     * @param targetPlayer The player's id to find and teleport to
     * @return True if successfully found the other player and teleported
     */
    public boolean tpToPlayer(PlayerModel playerModel, PlayerId targetPlayer) {
        return tpToPlayer(playerModel, targetPlayer.getName());
    }

    /**
     * Tp to another players whether they are on the same server or not
     *
     * @param playerModel The player to teleport
     * @param playerName  The player's name to find and teleport to
     * @return True if successfully found the other player and teleported
     */
    public boolean tpToPlayer(PlayerModel playerModel, String playerName) {
        Player targetPlayer = Bukkit.getPlayerExact(playerName);
        if (targetPlayer != null) {
            playerModel.getPlayer().teleport(targetPlayer);
            return true;
        }

        PlayerId targetPlayerId = PlayerListService.i().getOnlinePlayers().stream()
                .filter(p -> p.getName().equals(playerName))
                .findFirst().orElse(null);
        if (targetPlayerId != null && targetPlayerId.getServer() != null) {
            playerModel.getOnJoinData().setTpToPlayer(targetPlayerId.getName());
            tpToServer(playerModel, targetPlayerId.getServer());
            return true;
        }

        return false;
    }

    /**
     * Teleport a player to another server
     *
     * @param playerModel Player to teleport
     * @param serverId    Server id of the other server
     */
    public void tpToServer(PlayerModel playerModel, String serverId) {
        if (!serverId.equals(GlobalPlugin.i().getServerId())) {
            playerModel.saveTpState(false);

            TpHandler handler = new TpHandler();
            handler.setUuid(playerModel.getUuid());
            handler.setServerId(serverId);
            handler.setResponseConsumer(message -> {
                if (!message.isOk()) {
                    playerModel.sendMessage("Can't connect to server " + serverId, MessageType.ERROR);
                }
            });

            handler.send(Action.POST);
        }
    }
}
