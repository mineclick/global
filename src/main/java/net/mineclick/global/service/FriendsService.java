package net.mineclick.global.service;

import net.md_5.bungee.api.chat.TextComponent;
import net.mineclick.core.messenger.Action;
import net.mineclick.global.messenger.FriendsHandler;
import net.mineclick.global.messenger.TexturesHandler;
import net.mineclick.global.model.ChatSenderData;
import net.mineclick.global.model.FriendsData;
import net.mineclick.global.model.PlayerId;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.type.Rank;
import net.mineclick.global.util.MessageType;
import net.mineclick.global.util.SingletonInit;
import net.mineclick.global.util.Strings;
import net.mineclick.global.util.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@SingletonInit
public class FriendsService {
    private static FriendsService i;

    public static FriendsService i() {
        return i == null ? i = new FriendsService() : i;
    }

    /**
     * Send a friend request
     *
     * @param playerModel PlayerModel of the player who sent the request
     * @param toName      Name of the recipient
     */
    public void sendRequest(PlayerModel playerModel, String toName) {
        Long lastSent = playerModel.getFriendsData().getRequestsCooldown().get(toName);
        if (lastSent != null) {
            playerModel.sendMessage("You need to wait 5 minutes before sending a friend request to " + toName + " again", MessageType.ERROR);
            return;
        }

        findUuid(toName, toUuid -> {
            if (toUuid != null) {
                playerModel.getFriendsData().getRequestsCooldown().put(toName, System.currentTimeMillis());
                if (canSendTo(playerModel, toUuid, toName)) {
                    Runnable onSent = () -> {
                        playerModel.getFriendsData().getSentRequests().add(toUuid);
                        playerModel.updateFriends();
                        playerModel.sendMessage("Friend request send to " + toName, MessageType.INFO);
                    };

                    ChatSenderData from = ChatSenderData.from(playerModel);
                    if (handleRequest(from, toUuid)) {
                        onSent.run();
                    } else {
                        FriendsHandler handler = new FriendsHandler();
                        handler.setActionType(FriendsHandler.ActionType.REQUEST);
                        handler.setSenderData(from);
                        handler.setRecipient(toUuid);
                        handler.setResponseConsumer(message -> {
                            if (message != null) {
                                onSent.run();
                            } else {
                                playerModel.sendMessage("Can't send a friend request to " + toName + ", they are offline", MessageType.ERROR);
                            }
                        });
                        handler.send();
                    }
                }
                return;
            }

            playerModel.sendMessage("Invalid player name: " + toName, MessageType.ERROR);
        });
    }

    /**
     * Handle a friends request
     *
     * @param from   ChatSenderData of the player that sent the request
     * @param toUuid UUID of the recipient
     * @return True if the recipient is on this server
     */
    public boolean handleRequest(ChatSenderData from, UUID toUuid) {
        return !PlayersService.i().get(toUuid, toPlayerModel -> {
            toPlayerModel.getFriendsData().getReceivedRequests().add(from.getUuid());
            toPlayerModel.updateFriends();

            Player player = toPlayerModel.getPlayer();
            if (player != null) {
                TextComponent text = Strings.playerHoverInfo(from);
                text.addExtra(TextComponent.fromLegacyText(ChatColor.GOLD + " sent you a friend request")[0]);
                text = Strings.middle(text);

                player.sendMessage(Strings.line());
                player.spigot().sendMessage(text);
                player.sendMessage(" ");
                player.spigot().sendMessage(Strings.createButtons(toPlayerModel, "Accept", "Deny",
                        () -> answerRequest(toPlayerModel, from, true),
                        () -> answerRequest(toPlayerModel, from, false)));
                player.sendMessage(Strings.line());
            }
        }).isNull();
    }

    /**
     * Answer a friend request
     *
     * @param playerModel The player who accepts the request
     * @param from        PlayerId of the player that sent the request
     * @param accept      Whether to accept or deny the request
     */
    public void answerRequest(PlayerModel playerModel, PlayerId from, boolean accept) {
        FriendsData friendsData = playerModel.getFriendsData();
        if (friendsData.getFriends().contains(from.getUuid())) {
            playerModel.sendMessage("Already accepted " + from.getName() + "'s friend request", MessageType.ERROR);
            return;
        }

        if (!friendsData.getReceivedRequests().contains(from.getUuid())) {
            playerModel.sendMessage("There are no friend requests from " + from.getName(), MessageType.ERROR);
            return;
        }

        friendsData.getReceivedRequests().remove(from.getUuid());
        friendsData.getSentRequests().remove(from.getUuid());
        if (accept) {
            friendsData.getFriends().add(from.getUuid());
        }
        playerModel.updateFriends();
        playerModel.sendMessage((accept ? "Accepted " : "Denied ") + from.getName() + "'s friend request", MessageType.INFO);

        ChatSenderData senderData = ChatSenderData.from(playerModel);
        if (!handleAnswer(senderData, from.getUuid(), accept)) {
            FriendsHandler handler = new FriendsHandler();
            handler.setActionType(accept ? FriendsHandler.ActionType.ACCEPT : FriendsHandler.ActionType.DENY);
            handler.setSenderData(senderData);
            handler.setRecipient(from.getUuid());
            handler.send();

            // Handle timout (not online) and set the data manually
            handler.setResponseConsumer(message -> {
                if (message == null) {
                    PlayersService.i().edit(from.getUuid(), p -> {
                        p.getFriendsData().getReceivedRequests().remove(playerModel.getUuid());
                        if (accept) {
                            p.getFriendsData().getSentRequests().remove(playerModel.getUuid());
                            p.getFriendsData().getFriends().add(playerModel.getUuid());
                        }
                    });
                }
            });
        }
    }

    /**
     * Handler a request answer
     *
     * @param from   ChatSenderData of the player who answered the request
     * @param toUuid UUID of the player who sent the request
     * @param accept Whether the request was accepted or not
     * @return True if the player who sent the request is on this server
     */
    public boolean handleAnswer(ChatSenderData from, UUID toUuid, boolean accept) {
        return !PlayersService.i().get(toUuid, toPlayerModel -> {
            toPlayerModel.getFriendsData().getReceivedRequests().remove(from.getUuid());
            if (accept) {
                toPlayerModel.getFriendsData().getSentRequests().remove(from.getUuid());
                toPlayerModel.getFriendsData().getFriends().add(from.getUuid());
            }
            toPlayerModel.updateFriends();

            Player player = toPlayerModel.getPlayer();
            if (player == null) return;

            TextComponent textComponent = Strings.playerHoverInfo(from);
            if (accept) {
                textComponent.addExtra(ChatColor.GOLD + " accepted your friend request");
                player.spigot().sendMessage(textComponent);
            } else if (toPlayerModel.getRank().isAtLeast(Rank.STAFF)) {
                textComponent.addExtra(ChatColor.RED + " denied your friend request");
                player.spigot().sendMessage(textComponent);
            }
        }).isNull();
    }

    /**
     * Cancels a friend request
     *
     * @param playerModel PlayerModel of the player who sent the request
     * @param from        PlayerId of the player who received the request
     */
    public void cancelRequest(PlayerModel playerModel, PlayerId from) {
        FriendsData friendsData = playerModel.getFriendsData();
        if (friendsData.getSentRequests().remove(from.getUuid())) {
            playerModel.updateFriends();

            if (!handleCancel(playerModel, from.getUuid())) {
                FriendsHandler handler = new FriendsHandler();
                handler.setActionType(FriendsHandler.ActionType.CANCEL);
                handler.setSenderData(ChatSenderData.from(playerModel));
                handler.setRecipient(from.getUuid());
                handler.send();

                // Handle timout (not online) and set the data manually
                handler.setResponseConsumer(message -> {
                    if (message == null) {
                        PlayersService.i().edit(from.getUuid(), p ->
                                p.getFriendsData().getReceivedRequests().remove(playerModel.getUuid())
                        );
                    }
                });
            }
        }
    }

    /**
     * Handle a request cancellation
     *
     * @param from   PlayerId of the player who sent the request
     * @param toUuid UUID of the player who received the request
     * @return True if the player was on this server
     */
    public boolean handleCancel(PlayerId from, UUID toUuid) {
        return !PlayersService.i().get(toUuid, toPlayerModel -> {
                    toPlayerModel.getFriendsData().getReceivedRequests().remove(from.getUuid());
                    toPlayerModel.updateFriends();
                }
        ).isNull();
    }

    /**
     * Remove a friend
     *
     * @param playerModel PlayerModel of the player who wants to remove their friend
     * @param friend      PlayerId of the friend
     */
    public void removeFriend(PlayerModel playerModel, PlayerId friend) {
        FriendsData friendsData = playerModel.getFriendsData();
        if (friendsData.getFriends().remove(friend.getUuid())) {
            playerModel.updateFriends();

            if (!handleRemove(playerModel, friend.getUuid())) {
                FriendsHandler handler = new FriendsHandler();
                handler.setActionType(FriendsHandler.ActionType.REMOVE);
                handler.setSenderData(ChatSenderData.from(playerModel));
                handler.setRecipient(friend.getUuid());
                handler.send();

                // Handle timeout (not online) and set the data manually
                handler.setResponseConsumer(message -> {
                    if (message == null) {
                        PlayersService.i().edit(friend.getUuid(), p ->
                                p.getFriendsData().getFriends().remove(playerModel.getUuid())
                        );
                    }
                });
            }
        }
    }

    /**
     * Handle friend removal
     *
     * @param from   PlayerId of the player who sent the request
     * @param toUuid UUID of the friend to be removed
     * @return True if the player was on this server
     */
    public boolean handleRemove(PlayerId from, UUID toUuid) {
        return !PlayersService.i().get(toUuid, toPlayerModel -> {
            toPlayerModel.getFriendsData().getFriends().remove(from.getUuid());
            toPlayerModel.updateFriends();

            if (toPlayerModel.isRankAtLeast(Rank.STAFF)) {
                toPlayerModel.sendMessage(from.getName() + " removed you from friends", MessageType.INFO);
            }
        }).isNull();
    }

    /**
     * Query mongo to map uuids to player textures.
     * No caching implemented atm
     *
     * @param uuidSet  The uuids
     * @param consumer The consumer
     */
    public void getTextures(Set<UUID> uuidSet, Consumer<Map<UUID, String>> consumer) {
        Map<UUID, String> map = uuidSet.stream().collect(Collectors.toMap(uuid -> uuid, uuid -> ""));

        TexturesHandler handler = new TexturesHandler();
        handler.setTextures(map);
        handler.send(Action.GET);
        handler.setResponseConsumer(message -> {
            TexturesHandler response = (TexturesHandler) message;
            if (response == null || response.getTextures() == null) {
                consumer.accept(new HashMap<>());
                return;
            }

            consumer.accept(response.getTextures());
        });
    }

    private void findUuid(String name, Consumer<UUID> consumer) {
        Player player = Bukkit.getPlayerExact(name);
        if (player != null) {
            consumer.accept(player.getUniqueId());
        } else {
            UUIDFetcher.getUUID(name, consumer);
        }
    }

    private boolean canSendTo(PlayerModel playerModel, UUID uuidTo, String nameTo) {
        FriendsData friendsData = playerModel.getFriendsData();
        if (friendsData.getSentRequests().contains(uuidTo)) {
            playerModel.sendMessage("Already sent a friend request to " + nameTo, MessageType.ERROR);
            return false;
        }
        if (friendsData.getSentRequests().size() > FriendsData.MAX_FRIENDS) {
            playerModel.sendMessage("You sent too many friend requests", MessageType.ERROR);
            return false;
        }

        if (friendsData.getFriends().contains(uuidTo)) {
            playerModel.sendMessage("Already friends with " + nameTo, MessageType.ERROR);
            return false;
        }
        if (friendsData.getFriends().size() > FriendsData.MAX_FRIENDS) {
            playerModel.sendMessage("You have too many friends", MessageType.ERROR);
            return false;
        }
        return true;
    }

    public void checkCooldown(PlayerModel playerModel) {
        playerModel.getFriendsData().getRequestsCooldown().values()
                .removeIf(at -> System.currentTimeMillis() - at > 5 * 60000); // 5 min cooldown
    }
}
