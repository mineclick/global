package net.mineclick.global.service;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.global.Constants;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.messenger.PlayersHandler;
import net.mineclick.global.messenger.ServersHandler;
import net.mineclick.global.messenger.TpHandler;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.model.ServerModel;
import net.mineclick.global.type.ServerStatus;
import net.mineclick.global.util.Runner;
import net.mineclick.global.util.SingletonInit;
import net.mineclick.global.util.Strings;
import net.minecraft.network.PacketSendListener;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.game.ClientboundDisconnectPacket;
import net.minecraft.server.network.ServerGamePacketListenerImpl;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.craftbukkit.v1_20_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_20_R1.util.CraftChatMessage;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.generator.ChunkGenerator;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Getter
@SingletonInit
public class ServersService implements Listener {
    private static ServersService i;

    private final Set<ServerModel> networkServers = new HashSet<>();
    private final Instant startedOn;
    private final Instant restartsOn;
    private final Set<Runnable> onShutdownRunnables = new HashSet<>();
    @Setter
    private boolean isTestNet = !Constants.PROD;
    private boolean shuttingDown;
    @Setter
    private ServerStatus status = ServerStatus.STARTING;
    @Setter
    private boolean game = true;

    private ServersService() {
        startedOn = Instant.now();
        int lifetime = GlobalPlugin.i().getConfig().getInt("restart") + GlobalPlugin.random.nextInt(72000);
        restartsOn = Instant.now().plus(lifetime / 20, ChronoUnit.SECONDS);
        Bukkit.getPluginManager().registerEvents(this, GlobalPlugin.i());

        // Note: There's kinda of a bug in ClassLoaded where if the Global plugin doesn't load the
        //  net.mineclick.core.messenger.Action enum first it will fail to use it from here on.
        //  Here is probably the only place where the Action enum is used when the server
        //  first starts (before Game gets to load up), so keep this note in mind if you're going to
        //  change the initial delay or something else here.
        Runner.sync(0, 60, state -> {
            // Update every 3 seconds
            update();

            // Expire servers after 10 seconds of no updates
            Instant now = Instant.now();
            networkServers.removeIf(s -> s.getExpiresAt().isBefore(now));
        });

        //Restart server after some time
        if (!isTestNet) {
            Runner.sync(lifetime, () -> {
                if (!isTestNet) {
                    gracefulShutdown();
                }
            });
        }
    }

    public static ServersService i() {
        return i == null ? i = new ServersService() : i;
    }

    private ServerModel getThisServer() {
        ServerModel serverModel = new ServerModel();
        serverModel.setId(GlobalPlugin.i().getServerId());
        serverModel.setPlayers(Bukkit.getOnlinePlayers().size());
        serverModel.setStartedOn(startedOn);
        serverModel.setCpuLoad(getCpuLoad());
        serverModel.setRamUsage(getRamUsage());
        serverModel.setStatus(status);
        serverModel.setRestartsOn(restartsOn);
        serverModel.setGame(game);

        return serverModel;
    }

    /**
     * Broadcast the server's status
     */
    public void update() {
        ServersHandler handler = new ServersHandler();
        handler.setServer(getThisServer());
        handler.send(Action.POST);
    }

    /**
     * Handler other server's status update
     *
     * @param server The server that updated
     */
    public void handleServer(ServerModel server) {
        networkServers.remove(server);
        if (server.getStatus().isRunning()) {
            networkServers.add(server);
        }
    }

    /**
     * Get all servers on the network including this one
     *
     * @return Sorted array list of network servers
     */
    public List<ServerModel> getRunningServers(boolean gameServersOnly) {
        List<ServerModel> servers = new ArrayList<>(networkServers);
        servers.add(getThisServer());
        servers = servers.stream()
                .filter(server -> server.getStatus().isRunning() && (!gameServersOnly || server.isGame()))
                .collect(Collectors.toList());
        servers.sort(Comparator.comparing(ServerModel::getId));

        return servers;
    }

    /**
     * Get a network server with the least number of players.
     * Will only return game servers.
     *
     * @return The least populated server or null if no network servers present
     */
    public ServerModel getLowestServer() {
        return networkServers.stream()
                .filter(s -> s.getStatus().equals(ServerStatus.RUNNING) && s.isGame())
                .min(Comparator.comparing(ServerModel::getPlayers)).orElse(null);
    }

    @SneakyThrows
    private int getCpuLoad() {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = ObjectName.getInstance("java.lang:type=OperatingSystem");
        Double value = ((Double) mbs.getAttribute(name, "ProcessCpuLoad"));

        if (value == -1.0) {
            return 0;
        }

        return (int) (value * 100);
    }

    private int getRamUsage() {
        double max = Runtime.getRuntime().maxMemory();
        double free = Runtime.getRuntime().freeMemory();

        return (int) ((max - free) / max * 100);
    }

    public ChunkGenerator getDefaultWorldGenerator() {
        return new ChunkGenerator() {
            @Override
            public ChunkData generateChunkData(World world, Random random, int x, int z, BiomeGrid biome) {
                for (int i = 0; i < 16; i++) {
                    for (int j = 0; j < 16; j++) {
                        biome.setBiome(i, j, Biome.PLAINS);
                    }
                }
                return createChunkData(world);
            }

            @Override
            public final Location getFixedSpawnLocation(World paramWorld, Random paramRandom) {
                return new Location(paramWorld, 0, 100, 0);
            }
        };
    }

    @EventHandler
    public void on(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    private boolean canRestart() {
        return networkServers.stream().anyMatch(s -> s.getStatus().equals(ServerStatus.RUNNING));
    }

    /**
     * Creates a shutdown thread used for the shutdown hook.
     *
     * @return A new thread for shutting down.
     */
    public Thread createShutdownHook() {
        return new Thread(() -> {
            shutDown(true);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Add a runnable to run when the server is about to be shutdown
     *
     * @param runnable The runnable
     */
    public void onShutdown(Runnable runnable) {
        onShutdownRunnables.add(runnable);
    }

    /**
     * Start the graceful shutdown process. First check if another server exists to teleport the players.
     * Update the server status and finally shutdown after some delay.
     */
    public void gracefulShutdown() {
        if (shuttingDown)
            return;

        shuttingDown = true;
        status = ServerStatus.WAITING_TO_RESTART;
        update();

        Runner.sync(0, 200, new Consumer<>() {
            int staleTicks = 0;

            @Override
            public void accept(Runner.State state) {
                if (!ServersService.this.canRestart() && staleTicks++ < 20) {
                    GlobalPlugin.i().getLogger().info("Can't shutdown gracefully (" + staleTicks + "/10)");
                    return;
                }
                state.cancel();

                Bukkit.broadcastMessage(Strings.line());
                Bukkit.broadcastMessage(ChatColor.RED + "Server will restart in 5 seconds");
                Bukkit.broadcastMessage(ChatColor.GOLD + "Your progress will be saved!");
                Bukkit.broadcastMessage(Strings.line());

                Runner.sync(100, () -> {
                    Bukkit.broadcastMessage(Strings.line());
                    Bukkit.broadcastMessage(ChatColor.RED + "Server is restarting...");
                    Bukkit.broadcastMessage(ChatColor.GOLD + "Your progress will be saved!");
                    Bukkit.broadcastMessage(Strings.line());

                    // Players saved on quit so we're not doing anything here
                    Runner.sync(40, () -> {
                        ServersService.this.shutDown(false);

                        Runner.sync(40, Bukkit::shutdown);
                    });
                });
            }
        });
    }

    private void shutDown(boolean sendMsg) {
        GlobalPlugin.i().getLogger().info("Shutting down...");
        onShutdownRunnables.forEach(Runnable::run);

        List<PlayerModel> players = PlayersService.i().getAll();
        if (!players.isEmpty()) {
            // Save all player's data
            String msg = sendMsg ? ChatColor.RED + "The server you were on has shutdown.\n" + ChatColor.RED + "Don't worry, your progress was saved" : null;
            players.stream()
                    .filter(p -> !p.isOffline())
                    .peek(p -> {
                        p.getOnJoinData().setMsg(msg);
                        p.saveTpState(true);
                        p.setUpdatedAt(Instant.now());
                    })
                    .map(PlayersHandler::save)
                    .forEach(Message::blockResponse);

            // Try to teleport all players to a different server
            ServerModel server = getLowestServer();
            if (server != null) {
                GlobalPlugin.i().getLogger().info("Found a different server: " + server.getId());

                players.forEach(playerModel -> {
                    TpHandler handler = new TpHandler();
                    handler.setUuid(playerModel.getUuid());
                    handler.setServerId(server.getId());
                    handler.send(Action.POST);
                    handler.blockResponse();
                });

                GlobalPlugin.i().getLogger().info("All " + players.size() + " players were teleported");
            } else {
                GlobalPlugin.i().getLogger().warning("Couldn't find a different server!");

                Component message = CraftChatMessage.fromString(ChatColor.RED + "The MineClick server is restarting.\n \n" +
                        ChatColor.DARK_GREEN + "Don't worry, your progress was saved.\n" +
                        ChatColor.DARK_GREEN + "Please rejoin shortly", true)[0];
                players.forEach(p -> {
                    Player player = p.getPlayer();
                    if (player == null) return;

                    ServerGamePacketListenerImpl connection = ((CraftPlayer) player).getHandle().connection;
                    if (connection != null) {
                        connection.send(new ClientboundDisconnectPacket(message), PacketSendListener.thenRun(() -> {
                            connection.disconnect(message);
                        }));
                    }
                });
            }
        }

        status = ServerStatus.RESTARTING;
        update();
    }
}
