package net.mineclick.global.service;

import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.commands.Commands;
import net.mineclick.global.messenger.ChatHandler;
import net.mineclick.global.model.ChatData;
import net.mineclick.global.model.ChatSenderData;
import net.mineclick.global.model.PlayerId;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.type.PunishmentType;
import net.mineclick.global.type.Rank;
import net.mineclick.global.util.Formatter;
import net.mineclick.global.util.MessageType;
import net.mineclick.global.util.SingletonInit;
import net.mineclick.global.util.Strings;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerCommandSendEvent;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@SingletonInit
public class ChatService implements Listener {
    private static ChatService i;

    private Map<String, String> swears = new HashMap<>();

    private ChatService() {
        Bukkit.getPluginManager().registerEvents(this, GlobalPlugin.i());
        loadConfig(GlobalPlugin.i().getConfig());
    }

    public static ChatService i() {
        return i == null ? i = new ChatService() : i;
    }

    private void loadConfig(Configuration config) {
        swears = config.getStringList("swears").stream().collect(Collectors.toMap(s -> s.split(":")[0], s -> s.split(":")[1]));
    }

    /**
     * Send a public message to all players
     *
     * @param from The sender
     * @param msg  The message
     */
    public void sendPublicChat(PlayerModel from, String msg) {
        Player player = from.getPlayer();
        if (player == null)
            return;

        ChatData chatData = from.getChatData();
        if (chatData.getDisabled().get()) {
            player.sendMessage(ChatColor.RED + "You disabled your chat! Enable in the Main Menu");
            return;
        }
        if (OffencesService.i().isCurrentlyPunishedWith(from, PunishmentType.MUTE)) {
            sendMutedMessage(from);
            return;
        }

        //TODO about the profanity filter. Continue using this basic method while
        // working on the python docker image that will do a second pass on the message
        // and flag the user as abusive + mute/ban
        String original = msg;
        String[] split = msg.split(" ");
        for (int index = 0; index < split.length; index++) {
            String replace = swears.get(split[index].toLowerCase());
            if (replace != null) {
                split[index] = replace;
            }
        }
        msg = StringUtils.join(split, " ");

        if (!from.isRankAtLeast(Rank.STAFF)) {
            String lastMessage = msg.replaceAll("\\P{L}", "").toLowerCase();
            if (chatData.getLastMessage() != null && lastMessage.equals(chatData.getLastMessage())) {
                player.sendMessage(ChatColor.RED + "You can't say the same thing twice");
                return;
            }
            chatData.setLastMessage(lastMessage);

            int upperCount = 0;
            char[] chars = msg.toCharArray();
            for (int i = 1; i < chars.length; i++) {
                char c = chars[i];
                if (Character.isUpperCase(c) && c != 'I') {
                    upperCount++;
                }
            }
            if (upperCount > chars.length / 4) {
                msg = msg.toLowerCase();
            }
        }

        from.getChatData().addHistory(original);

        ChatSenderData senderData = ChatSenderData.from(from);
        boolean staffChat = from.isRankAtLeast(Rank.STAFF) && (msg.startsWith("!") || from.getChatData().isStaffChat());
        if (staffChat) {
            if (msg.startsWith("!")) {
                msg = msg.substring(1);
            }
            handleStaffChat(senderData, msg);
        } else {
            handlePublicChat(senderData, msg);
        }

        ChatHandler handler = new ChatHandler();
        handler.setChatType(staffChat ? ChatHandler.ChatType.STAFF : ChatHandler.ChatType.PUBLIC);
        handler.setSenderData(senderData);
        handler.setMessage(msg);
        handler.send();
    }

    /**
     * Send private message to a player
     *
     * @param from    The sender
     * @param message The message
     * @param to      The recipient name
     */
    public void sendPrivateChat(PlayerModel from, String to, String message) {
        PlayerId toPlayerId = PlayerListService.i().getOnlinePlayers().stream()
                .filter(playerId -> playerId.getName().equalsIgnoreCase(to)).findFirst().orElse(null);

        if (toPlayerId == null) {
            from.sendMessage(to + " is offline", MessageType.ERROR);
            return;
        }

        ChatSenderData senderData = ChatSenderData.from(from);
        Player toPlayer = Bukkit.getPlayerExact(to);
        if (toPlayer != null) {
            PlayersService.i().get(toPlayer.getUniqueId(), playerModelTo -> {
                if (!handlePrivateChat(senderData, message, playerModelTo) && from.getPlayer() != null) {
                    from.sendMessage("Can't send private messages to " + to, MessageType.ERROR);
                } else if (!from.isOffline()) {
                    from.getPlayer().spigot().sendMessage(preparePrivateMessage(ChatSenderData.from(from), toPlayerId, message));
                }
            });
        } else {
            from.getPlayer().spigot().sendMessage(preparePrivateMessage(ChatSenderData.from(from), toPlayerId, message));

            ChatHandler handler = new ChatHandler();
            handler.setChatType(ChatHandler.ChatType.PRIVATE);
            handler.setSenderData(senderData);
            handler.setRecipientName(to);
            handler.setMessage(message);
            handler.send();
        }
    }

    public boolean handlePrivateChat(ChatSenderData from, String message, PlayerModel to) {
        Player player = to.getPlayer();
        if (player == null || (to.getChatData().getPrivateFromFriendsOnly().get() && !to.getFriendsData().isFriendsWith(from.getUuid())) && !from.getRank().isAtLeast(Rank.STAFF)) {
            return false;
        }

        player.spigot().sendMessage(preparePrivateMessage(from, null, message));
        return true;
    }

    private TextComponent preparePrivateMessage(ChatSenderData from, PlayerId to, String message) {
        if (from.getRank().isAtLeast(Rank.STAFF)) {
            message = ChatColor.translateAlternateColorCodes('&', message);
        }
        message = ": " + ChatColor.WHITE + message;

        boolean toSender = to != null;
        TextComponent text = new TextComponent(TextComponent.fromLegacyText(ChatColor.LIGHT_PURPLE + (toSender ? "TO " : "FROM ")));
        text.addExtra(Strings.playerHoverInfo(toSender ? ChatSenderData.from(to) : from, ChatColor.AQUA + (toSender ? "Click to message" : "Click to reply"), "/msg", false));
        text.addExtra(message);

        return text;
    }

    /**
     * Broadcast a message to all players on all servers
     *
     * @param message       The message
     * @param secondMessage Optional secondary message (gray).
     * @param important     Whether the message is important: encapsulated and centered
     */
    public void sendBroadcast(String message, String secondMessage, boolean important) {
        handleBroadcast(message, secondMessage, important);

        ChatHandler handler = new ChatHandler();
        handler.setChatType(ChatHandler.ChatType.BROADCAST);
        handler.setMessage(message);
        handler.setSecondMessage(secondMessage);
        handler.setImportant(important);
        handler.send();
    }

    public void handlePublicChat(ChatSenderData from, String message) {
        TextComponent text = format(from, message);

        PlayersService.i().forAll(player -> {
            if (player.isOffline() || player.getChatData().getDisabled().get()) return;

            player.getPlayer().spigot().sendMessage(text);
        });
    }

    public void handleStaffChat(ChatSenderData from, String message) {
        TextComponent text;
        if (from != null) {
            text = new TextComponent(ChatColor.RED.toString() + ChatColor.BOLD + "|S|");
            text.addExtra(format(from, message));
        } else {
            text = new TextComponent(new ComponentBuilder(message).create());
        }

        PlayersService.i().forAll(player -> {
            if (!player.isRankAtLeast(Rank.STAFF) || player.isOffline() || player.getChatData().getDisabled().get()) {
                return;
            }

            player.getPlayer().spigot().sendMessage(text);
        });
    }

    private TextComponent format(ChatSenderData from, String message) {
        if (from.getRank().isAtLeast(Rank.STAFF)) {
            message = ChatColor.translateAlternateColorCodes('&', message);
        }
        message = ": " + (from.getRank().isAtLeast(Rank.PAID) ? ChatColor.WHITE : ChatColor.GRAY) + message;

        TextComponent text = Strings.playerHoverInfo(from);
        text.addExtra(message);

        return text;
    }

    public void handleBroadcast(String message, String secondMessage, boolean important) {
        StringBuilder builder = new StringBuilder();
        if (important) {
            builder.append(Strings.line()).append("\n");
            builder.append(Strings.middle(ChatColor.YELLOW + message));
        } else {
            builder.append(ChatColor.YELLOW).append(message);
        }

        if (secondMessage != null) {
            builder.append("\n ").append("\n").append(Strings.middle(ChatColor.GRAY + secondMessage));
        }
        if (important) {
            builder.append("\n").append(Strings.line());
        }

        PlayersService.i().forAllBukkit(player -> player.sendMessage(builder.toString()));
    }

    /**
     * Send muted message to the player (if they are actually muted)
     *
     * @param playerModel The PlayerModel
     */
    public void sendMutedMessage(PlayerModel playerModel) {
        Player player = playerModel.getPlayer();
        if (player == null)
            return;

        Instant until = OffencesService.i().getExpiresOn(playerModel, PunishmentType.MUTE);
        if (until != null) {
            long millis = until.toEpochMilli() - Instant.now().toEpochMilli();
            player.sendMessage(ChatColor.RED + "You are still muted for " + Formatter.duration(millis));
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void on(AsyncPlayerChatEvent e) {
        e.setCancelled(true);
        Player player = e.getPlayer();

        PlayersService.i().get(player.getUniqueId(), playerModel -> sendPublicChat(playerModel, e.getMessage()))
                .ifNull(() -> e.getPlayer().sendMessage(ChatColor.RED + "Error loading your player data! Try to rejoin or contact staff"));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void on(PlayerCommandPreprocessEvent e) {
        String[] split = e.getMessage().split(" ");
        String command = split[0].replace("/", "").toLowerCase();

        e.setCancelled(true);
        PlayersService.i().get(e.getPlayer().getUniqueId(), playerModel -> {
            if (playerModel.getRank().isAtLeast(Rank.STAFF)) {
                e.setCancelled(false);
            }

            long currentTime = System.currentTimeMillis();
            if (currentTime - playerModel.getLastCommandExecutedAt() < 1000) {
                return;
            }
            playerModel.setLastCommandExecutedAt(currentTime);

            if (Commands.has(command)) {
                Commands.process(command, playerModel, split.length > 1 ? Arrays.copyOfRange(split, 1, split.length) : new String[]{});
            } else if (command.startsWith("?mc")) {
                playerModel.sendMessage("This action has expired", MessageType.ERROR);
            }
        });

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void on(PlayerCommandSendEvent e) {
        if (!ServersService.i().isGame()) return;

        AtomicBoolean skip = new AtomicBoolean();
        PlayersService.i().get(e.getPlayer().getUniqueId(), playerModel -> {
            if (playerModel.isRankAtLeast(Rank.SUPER_STAFF)) {
                skip.set(true);
            }
        });

        if (!skip.get()) {
            e.getCommands().removeIf(c -> !Commands.has(c));
        }
    }
}
