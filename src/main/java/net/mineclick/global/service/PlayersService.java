package net.mineclick.global.service;

import lombok.Setter;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.events.PlayerJoin;
import net.mineclick.global.messenger.OnlinePlayersHandler;
import net.mineclick.global.messenger.PlayersHandler;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.type.Rank;
import net.mineclick.global.util.IfNull;
import net.mineclick.global.util.Runner;
import net.mineclick.global.util.SingletonInit;
import net.mineclick.global.util.location.LocationParser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_20_R1.CraftServer;
import org.bukkit.craftbukkit.v1_20_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

@SingletonInit
public class PlayersService implements Listener {
    private static PlayersService i;
    private final Map<UUID, Object> cache = new ConcurrentHashMap<>();
    private final Set<UUID> loading = new HashSet<>();
    @Setter
    private Class<? extends PlayerModel> playerClass = PlayerModel.class;

    private PlayersService() {
        Bukkit.getPluginManager().registerEvents(this, GlobalPlugin.i());
    }

    public static PlayersService i() {
        return i == null ? i = new PlayersService() : i;
    }

    /**
     * Query the database and load the {@link net.mineclick.global.model.PlayerModel}
     * optionally caching or creating a new record if none exists
     *
     * @param uuid        The {@link UUID} of the player
     * @param cacheAndNew Whether to cache the loaded player and if none found, create a new one
     * @param consumer    Called after loading the player. Consumer value can be null
     *                    if the player doesn't exist or error occurs
     */
    public <T extends PlayerModel> void load(UUID uuid, boolean cacheAndNew, Consumer<T> consumer) {
        PlayersHandler.get(uuid, cacheAndNew, playerClass, player -> {
            if (player == null) {
                consumer.accept(null);
            } else {
                if (player.getUuid() == null) {
                    consumer.accept(null);
                    GlobalPlugin.i().getLogger().severe("Null PlayerModel UUID!");
                    return;
                }

                if (cacheAndNew) {
                    if (!player.load()) return;
                    T previous = (T) cache.put(uuid, player);
                    if (previous != null) {
                        previous.setDestroyed(true);
                    }
                }
                consumer.accept((T) player);
            }
        });
    }

    /**
     * Check if the player is loaded and stored in cache
     *
     * @param uuid The uuid of the player
     * @return True if the player was loaded
     */
    public boolean isLoaded(UUID uuid) {
        return !get(uuid, p -> {
        }).isNull();
    }

    /**
     * Apply a consumer for a cached player model.
     * <br>
     * If the PlayerModel doesn't exist, {@link PlayerConditional} is returned positive.
     * <br>
     * <br>
     * Chain {@link PlayerConditional#orLoad(boolean)} to load the player from DB and call the same consumer.
     * The loaded can still be null so the ifNull() can be used to catch it.
     * <br>
     * <br>
     * Example:
     * <pre>{@code
     *  PlayerService.i().get(uuid, playerModel -> {})
     *  .orLoad(true)
     *  .ifNull(() -> {
     *     throw new Exception("Player not found");
     *  });
     * }</pre>
     *
     * @param uuid     The {@link UUID} of the player
     * @param consumer {@link PlayerModel} consumer
     * @return {@link PlayerConditional}
     */
    public <T extends PlayerModel> PlayerConditional<T> get(UUID uuid, Consumer<T> consumer) {
        return get(uuid, consumer, false);
    }

    /**
     * Either edit cached model, or load and then save after consumer is called
     *
     * @param uuid     The {@link UUID} of the player
     * @param consumer {@link PlayerModel} consumer
     * @return {@link IfNull}
     */
    public <T extends PlayerModel> IfNull edit(UUID uuid, Consumer<T> consumer) {
        return get(uuid, consumer, true).orLoad(false);
    }

    private <T extends PlayerModel> PlayerConditional<T> get(UUID uuid, Consumer<T> consumer, boolean saveAfterLoad) {
        T playerModel = (T) cache.get(uuid);
        if (playerModel == null || playerModel.isDestroyed()) {
            cache.remove(uuid);

            return new PlayerConditional<>(uuid, loadedModel -> {
                consumer.accept(loadedModel);
                if (saveAfterLoad) {
                    save(loadedModel);
                }
            }, true);
        }

        consumer.accept(playerModel);
        return new PlayerConditional<>(false);
    }

    /**
     * Gets all cached {@link PlayerModel}'s
     *
     * @return {@link ArrayList} of PlayerModel's
     */
    public <T extends PlayerModel> List<T> getAll() {
        return new ArrayList(cache.values());
    }

    /**
     * Apply a consumer for all players
     *
     * @param consumer PlayerModel Consumer
     */
    public <T extends PlayerModel> void forAll(Consumer<T> consumer) {
        this.<T>getAll().forEach(consumer);
    }

    /**
     * Apply a consumer for all Bukkit {@link org.bukkit.entity.Player}s
     *
     * @param consumer Player Consumer
     */
    public void forAllBukkit(Consumer<Player> consumer) {
        getAll().stream()
                .map(PlayerModel::getPlayer)
                .filter(Objects::nonNull)
                .forEach(consumer);
    }

    /**
     * Saves the {@link PlayerModel} to the database
     *
     * @param playerModel The PlayerModel to be saved
     */
    public <T extends PlayerModel> void save(T playerModel) {
        playerModel.setUpdatedAt(Instant.now());
        PlayersHandler.save(playerModel);
    }

    /**
     * Removes the {@link PlayerModel} from cache if one exists
     *
     * @param uuid {@link UUID} of the PlayerModel to be removed
     * @return The removed PlayerModel or null if doesn't exist
     */
    public <T extends PlayerModel> T remove(UUID uuid) {
        T removed = (T) cache.remove(uuid);
        if (removed != null) {
            removed.setDestroyed(true);
        }
        return removed;
    }

    /**
     * Hide the player from all players and hide all players from this player
     *
     * @param player The player
     */
    public <T extends PlayerModel> void hideFromAll(T player) {
        hideFromAll(player, Collections.emptySet());
    }

    /**
     * Hide the player from all players and hide all players from this player.
     * Except certain players. To those the player will be shown and the player
     * will see those players
     *
     * @param player The player
     * @param except Except these players
     */
    public <T extends PlayerModel> void hideFromAll(T player, Collection<T> except) {
        Player bukkitPlayer = player.getPlayer();
        if (bukkitPlayer == null) {
            return;
        }

        this.<T>forAll(otherPlayer -> {
            Player bukkitOtherPlayer = otherPlayer.getPlayer();
            if (bukkitPlayer.equals(bukkitOtherPlayer)) return;

            if (bukkitOtherPlayer != null) {
                if (except.contains(otherPlayer)) {
                    bukkitOtherPlayer.showPlayer(GlobalPlugin.i(), bukkitPlayer);
                    bukkitPlayer.showPlayer(GlobalPlugin.i(), bukkitOtherPlayer);
                } else {
                    if (!otherPlayer.getRank().isAtLeast(Rank.STAFF)) {
                        bukkitOtherPlayer.hidePlayer(GlobalPlugin.i(), bukkitPlayer);
                    }
                    if (!player.getRank().isAtLeast(Rank.STAFF)) {
                        bukkitPlayer.hidePlayer(GlobalPlugin.i(), bukkitOtherPlayer);
                    }
                }
            }
        });
    }

    /**
     * Show the player to all players and show all players to this player
     *
     * @param player The player
     */
    public <T extends PlayerModel> void showToAll(T player) {
        Player bukkitPlayer = player.getPlayer();
        if (bukkitPlayer == null) {
            return;
        }

        forAllBukkit(p -> {
            p.showPlayer(GlobalPlugin.i(), bukkitPlayer);
            bukkitPlayer.showPlayer(GlobalPlugin.i(), p);
        });
    }

    @EventHandler
    public void on(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        if (!loading.add(e.getPlayer().getUniqueId())) {
            return;
        }

        // Teleport to the "loading" location
        e.getPlayer().teleport(LocationParser.parse("-999.5 102 -999.5 0 0"));
        e.getPlayer().setGameMode(GameMode.ADVENTURE);

        // In case the player is coming from another server,
        // give it a second to make sure their data is saved from there.
        Runner.sync(20, () -> {
            loading.remove(e.getPlayer().getUniqueId());
            if (!e.getPlayer().isOnline())
                return;

            // Bungee will not set player online on network join,
            // it will only set it offline when they leave.
            // We use this assumption to distinguish network/local logins
            get(e.getPlayer().getUniqueId(), playerModel -> Runner.sync(() -> {
                if (playerModel.isOffline()) return;

                boolean networkWide = !playerModel.isOnline(); // Here's the assumption from above

                Bukkit.getPluginManager().callEvent(new PlayerJoin(playerModel, networkWide));

                if (!networkWide && playerModel.getOnJoinData().getMsg() != null) {
                    playerModel.sendMessage(playerModel.getOnJoinData().getMsg());
                }
                playerModel.getOnJoinData().setMsg(null);

                playerModel.onJoin(networkWide);
                playerModel.setServer(GlobalPlugin.i().getServerId());
                if (networkWide) {
                    OnlinePlayersHandler.setOnline(playerModel);
                }

                // update commands
                ((CraftServer) GlobalPlugin.i().getServer()).getServer().getCommands().sendCommands(((CraftPlayer) playerModel.getPlayer()).getHandle());
            })).orLoad(true).ifNull(() ->
                    e.getPlayer().sendMessage(ChatColor.RED + "Error loading your data, please contact staff")
            );
        });
    }

    @EventHandler
    public void on(PlayerQuitEvent e) {
        e.setQuitMessage(null);

        UUID uuid = e.getPlayer().getUniqueId();
        Object model = cache.remove(uuid);
        if (model != null) {
            ((PlayerModel) model).onQuit();
            save((PlayerModel) model);
        }

        // It is assumed that Bungee will handle players leaving
        // the network properly, so we are not setting online = false here.
        // Additionally, PlayerModel should handle null Bukkit players itself.
    }

    public class PlayerConditional<T extends PlayerModel> extends IfNull {
        private final UUID uuid;
        private Consumer<T> loadConsumer = null;
        private boolean loaded;

        public PlayerConditional(UUID uuid, Consumer<T> loadConsumer, boolean isNull) {
            super(isNull);
            this.uuid = uuid;
            this.loadConsumer = loadConsumer;
        }

        public PlayerConditional(boolean isNull) {
            super(isNull);
            uuid = null;
        }

        /**
         * If the player isn't cached, it will be loaded from db
         *
         * @param cache Whether to cache the model after getting it from db
         * @return {@link IfNull} that can be chained further
         */
        public IfNull orLoad(boolean cache) {
            if (canBeLoaded()) {
                loaded = true;

                await();
                PlayersService.i().load(uuid, cache, p -> {
                    if (loadConsumer != null && p != null) {
                        loadConsumer.accept((T) p);
                    }

                    this.releaseAwait(p == null);
                    loadConsumer = null;
                });
            }
            return this;
        }

        /**
         * @return True if {@link PlayerConditional#orLoad(boolean)}
         * will actually load data if called
         */
        public boolean canBeLoaded() {
            return !loaded && uuid != null && loadConsumer != null && !loading.contains(uuid);
        }
    }
}
