package net.mineclick.global.service;

import lombok.Getter;
import net.mineclick.global.util.SingletonInit;
import org.bukkit.configuration.ConfigurationSection;

import java.util.List;

@SingletonInit
public class WhitelistService {
    private static WhitelistService i;

    @Getter
    private boolean enabled;
    private List<String> list;

    private WhitelistService() {
        ConfigurationsService.i().onUpdate("whitelist", this::updateConfig);
    }

    public static WhitelistService i() {
        return i == null ? i = new WhitelistService() : i;
    }

    private void updateConfig() {
        ConfigurationSection section = ConfigurationsService.i().get("whitelist");
        if (section == null) return;

        enabled = section.getBoolean("enabled");
        list = section.getStringList("list");
    }

    /**
     * Check if the whitelist contains the player's name
     *
     * @param name The player's name
     * @return True if whitelisted
     */
    public boolean contains(String name) {
        return list != null && list.contains(name);
    }
}
