package net.mineclick.global.util.ui;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.mineclick.global.util.ItemBuilder;
import net.mineclick.global.util.Skins;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class ItemUI {
    @Getter
    private final ItemStack item;
    @Getter
    @Setter
    private Consumer<InventoryClickPack> clickConsumer;
    @Getter
    @Setter
    private Consumer<ItemUI> updateConsumer;
    private String skin;

    private boolean changed;
    private Material oldType;
    private String oldName;
    private int oldAmount;
    private List<String> oldLore;
    private String oldSkin;
    private boolean oldGlowing;

    public ItemUI(@NonNull ItemBuilder.ItemBuilderBuilder builder, @NonNull Consumer<InventoryClickPack> clickConsumer) {
        this(builder.build().toItem(), clickConsumer);
    }

    public ItemUI(@NonNull ItemBuilder.ItemBuilderBuilder builder) {
        this(builder.build().toItem());
    }

    public ItemUI(@NonNull Material type, @NonNull Consumer<InventoryClickPack> clickConsumer) {
        this.item = new ItemStack(type);
        this.clickConsumer = clickConsumer;
    }

    public ItemUI(@NonNull Material type) {
        this.item = new ItemStack(type);
    }

    public ItemUI(@NonNull String skin, @NonNull Consumer<InventoryClickPack> clickConsumer) {
        this.item = new ItemStack(Material.PLAYER_HEAD);
        setSkin(skin);
        this.clickConsumer = clickConsumer;
    }

    public ItemUI(@NonNull String skin) {
        this.item = new ItemStack(Material.PLAYER_HEAD);
        setSkin(skin);
    }

    public ItemUI(@NonNull ItemStack item, @NonNull Consumer<InventoryClickPack> clickConsumer) {
        this.item = item;
        this.clickConsumer = clickConsumer;
    }

    public ItemUI(@NonNull ItemStack item) {
        this.item = item;
    }

    public void setMaterial(Material material) {
        if (material == null) {
            material = Material.AIR;
        }

        item.setType(material);
    }

    public void setTitle(String name) {
        ItemMeta meta = item.getItemMeta();
        if (meta == null) return;

        if (name == null) {
            name = "";
        }

        meta.setDisplayName(name);
        item.setItemMeta(meta);

        // remove attributes
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS);
        item.setItemMeta(meta);
    }

    public void setLore(String... lore) {
        ItemMeta meta = item.getItemMeta();
        if (meta == null) return;

        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
    }

    public void addLore(String lore) {
        ItemMeta meta = item.getItemMeta();
        if (meta == null) return;

        if (!meta.hasLore()) {
            setLore(lore);
            return;
        }

        List<String> loreList = meta.getLore();
        if (loreList != null) {
            loreList.add(lore);
        }
        meta.setLore(loreList);
        item.setItemMeta(meta);
    }

    public void setAmount(int amount) {
        amount = Math.max(1, amount);
        item.setAmount(amount);
    }

    public void setGlowing() {
        item.addEnchantment(ItemBuilder.GlowEnchant.get(), 1);
    }

    public void removeGlowing() {
        item.removeEnchantment(ItemBuilder.GlowEnchant.get());
    }

    public void setSkin(String skin) {
        if (item == null || !(item.getItemMeta() instanceof SkullMeta)) return;

        this.skin = skin;
        Skins.set(item, skin);
    }

    public boolean hasChanged() {
        // check amount
        if (item.getAmount() != oldAmount) {
            oldAmount = item.getAmount();
            changed = true;
        }

        // check type
        if (item.getType() != oldType) {
            oldType = item.getType();
            changed = true;
        }

        // check glowing
        boolean glowing = item.getEnchantments().containsKey(ItemBuilder.GlowEnchant.get());
        if (!Objects.equals(oldGlowing, glowing)) {
            oldGlowing = glowing;
            changed = true;
        }

        // check skin
        if (!Objects.equals(skin, oldSkin)) {
            oldSkin = skin;
            changed = true;
        }

        ItemMeta meta = item.getItemMeta();
        if (meta != null) {
            // check lore
            List<String> lore = item.getItemMeta().getLore();
            if (!Objects.equals(lore, oldLore)) {
                oldLore = lore;
                changed = true;
            }

            // check name
            if (!Objects.equals(meta.getDisplayName(), oldName)) {
                oldName = meta.getDisplayName();
                changed = true;
            }
        }

        if (changed) {
            changed = false;
            return true;
        }
        return false;
    }
}
