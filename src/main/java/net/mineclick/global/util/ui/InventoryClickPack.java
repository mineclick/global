package net.mineclick.global.util.ui;

import net.mineclick.global.model.PlayerModel;
import org.bukkit.event.inventory.InventoryClickEvent;

public record InventoryClickPack(PlayerModel playerModel, InventoryClickEvent event, ItemUI itemUI) {
}
