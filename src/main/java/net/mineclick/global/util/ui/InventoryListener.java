package net.mineclick.global.util.ui;

import net.mineclick.global.GlobalPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class InventoryListener implements Listener {
    public InventoryListener() {
        Bukkit.getPluginManager().registerEvents(this, GlobalPlugin.i());
    }

    @EventHandler
    public void onInventoryInteractEvent(InventoryClickEvent e) {
        InventoryUI.getActiveUIs().stream().filter(u -> u.hasAssociated(((Player) e.getWhoClicked()))).forEach(m -> m.onClick(e));
    }

    @EventHandler
    public void onInventoryCloseEvent(InventoryCloseEvent e) {
        InventoryUI.getActiveUIs().stream().filter(u -> u.hasAssociated(((Player) e.getPlayer()))).forEach(InventoryUI::onClose);
    }
}
