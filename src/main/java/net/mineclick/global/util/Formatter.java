package net.mineclick.global.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

public class Formatter {
    private static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#,###.##");
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            .withZone(ZoneId.systemDefault());

    public static String roundedTime(long time) {
        time /= 1000; //Convert to seconds

        if (time < (5 * 60)) { //Less than 5 minutes
            return "a few minutes";
        }

        if (time < (60 * 60)) { //Less than an hour
            return ((time / 60) % 60) + " minutes";
        }

        if (time < (60 * 60 * 24)) { //Less than a day
            return (time < (60 * 60 * 2) ? "over an hour" : (time / (60 * 60) % 24) + " hours");
        }

        if (time < (60 * 60 * 24 * 30)) { //Less than a month
            return (time < (60 * 60 * 24 * 2) ? "over a day" : (time / (60 * 60 * 24) % 30) + " days");
        }

        if (time < (60 * 60 * 24 * 30 * 12)) { //Less than a year
            return (time < (60 * 60 * 24 * 30 * 2) ? "over a month" : (time / (60 * 60 * 24 * 30) % 12) + " months");
        }

        return "over a year";
    }

    public static String duration(long time) {
        return duration(time, TimeUnit.MILLISECONDS);
    }

    public static String duration(long time, TimeUnit timeUnit) {
        return String.format("%02d:%02d:%02d", timeUnit.toHours(time),
                timeUnit.toMinutes(time) % TimeUnit.HOURS.toMinutes(1),
                timeUnit.toSeconds(time) % TimeUnit.MINUTES.toSeconds(1));
    }

    public static String durationWithMilli(long time) {
        return durationWithMilli(time, true);
    }

    public static String durationWithMilli(long time, boolean noZeroHours) {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        if (time <= 3600000 && noZeroHours) {
            return String.format("%02d:%02d.%02d", timeUnit.toMinutes(time),
                    timeUnit.toSeconds(time) % TimeUnit.MINUTES.toSeconds(1),
                    time % TimeUnit.SECONDS.toMillis(1) / 10);
        } else {
            return String.format("%02d:%02d:%02d.%02d", timeUnit.toHours(time),
                    timeUnit.toMinutes(time) % TimeUnit.HOURS.toMinutes(1),
                    timeUnit.toSeconds(time) % TimeUnit.MINUTES.toSeconds(1),
                    time % TimeUnit.SECONDS.toMillis(1) / 10);
        }
    }

    public static String exactDateTime(Instant instant) {
        return DATE_FORMAT.format(instant);
    }

    public static String format(double number) {
        return NUMBER_FORMAT.format(number);
    }
}
