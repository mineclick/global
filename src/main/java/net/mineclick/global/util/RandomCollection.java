package net.mineclick.global.util;

import net.mineclick.global.GlobalPlugin;

import java.util.Collection;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

public class RandomCollection<E> {
    private final NavigableMap<Double, E> map = new TreeMap<>();
    private final Random random = GlobalPlugin.random;
    private double total = 0;

    public RandomCollection<E> add(double weight, E result) {
        if (weight <= 0)
            return this;

        total += weight;
        map.put(total, result);
        return this;
    }

    public Collection<E> values() {
        return map.values();
    }

    public E next() {
        double value = random.nextDouble() * total;
        return map.higherEntry(value).getValue();
    }

    public void clear() {
        map.clear();
    }
}
