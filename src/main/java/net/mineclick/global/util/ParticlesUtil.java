package net.mineclick.global.util;

import net.mineclick.global.model.PlayerModel;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.network.protocol.game.ClientboundLevelParticlesPacket;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.craftbukkit.v1_20_R1.CraftParticle;
import org.joml.Vector3f;

import java.awt.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class ParticlesUtil {
    public static void send(ParticleOptions particle, Location location, Triple<Float, Float, Float> offset, int count, PlayerModel... players) {
        send(particle, location, offset, 0, count, players);
    }

    public static void send(ParticleOptions particle, Location location, Triple<Float, Float, Float> offset, int count, Collection<? extends PlayerModel> players) {
        send(particle, location, offset, 0, count, players);
    }

    public static void send(ParticleOptions particle, Location location, Triple<Float, Float, Float> offset, float extra, int count, PlayerModel... players) {
        send(particle, location, offset, extra, count, Arrays.asList(players));
    }

    public static void send(ParticleOptions particle, Location location, Triple<Float, Float, Float> offset, float extra, int count, Collection<? extends PlayerModel> players) {
        ClientboundLevelParticlesPacket packet = new ClientboundLevelParticlesPacket(particle, false, location.getX(), location.getY(), location.getZ(), offset.first(), offset.second(), offset.third(), extra, count);
        for (PlayerModel p : players) {
            p.sendPacket(packet);
        }
    }

    public static void sendColor(Location location, Color color, PlayerModel player) {
        sendColor(location, color.getRed(), color.getGreen(), color.getBlue(), Collections.singleton(player));
    }

    public static void sendColor(Location location, Color color, Collection<? extends PlayerModel> players) {
        sendColor(location, color.getRed(), color.getGreen(), color.getBlue(), players);
    }

    public static void sendColor(Location location, int red, int green, int blue, PlayerModel player) {
        sendColor(location, red, green, blue, Collections.singleton(player));
    }

    public static void sendColor(Location location, double red, double green, double blue, Collection<? extends PlayerModel> players) {
        DustParticleOptions particle = new DustParticleOptions(new Vector3f((float) (red / 255), (float) (green / 255), (float) (blue / 255)), 1);
        ClientboundLevelParticlesPacket packet = new ClientboundLevelParticlesPacket(particle, true, location.getX(), location.getY(), location.getZ(), 0, 0, 0, 1, 0);
        for (PlayerModel p : players) {
            p.sendPacket(packet);
        }
    }

    public static void sendBlock(Location location, org.bukkit.Material material, Triple<Float, Float, Float> offset, float extra, int count, Collection<? extends PlayerModel> players) {
        ParticleOptions particle = CraftParticle.toNMS(Particle.BLOCK_DUST, material.createBlockData());
        ClientboundLevelParticlesPacket packet = new ClientboundLevelParticlesPacket(particle, true, location.getX(), location.getY(), location.getZ(), offset.first(), offset.second(), offset.third(), extra, count);
        for (PlayerModel player : players) {
            player.sendPacket(packet);
        }
    }
}
