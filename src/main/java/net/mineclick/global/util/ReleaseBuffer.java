package net.mineclick.global.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ReleaseBuffer<E> {
    private final List<E> list = new ArrayList<>();
    private int tickCount = 0;

    public ReleaseBuffer(Consumer<List<E>> forEachOnRelease, int ticks) {
        Runner.sync(0, 1, state -> {
            if (tickCount++ >= ticks) {
                state.cancel();
                forEachOnRelease.accept(list);
                list.clear();
            }
        });
    }

    public void add(E e) {
        list.add(e);
        tickCount = 0;
    }
}
