package net.mineclick.global.util;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;

public class ChunkPreload {
    public static void preload(Chunk chunk) {
        preload(chunk.getX(), chunk.getZ(), 1);
    }

    public static void preload(Chunk chunk, int radius) {
        preload(chunk.getX(), chunk.getZ(), radius);
    }

    public static void preload(int chunkX, int chunkZ, int radius) {
        World world = Bukkit.getWorlds().get(0);
        for (int x = chunkX - radius; x <= chunkX + radius; x++) {
            for (int z = chunkZ - 1; z <= chunkZ; z++) {
                world.getChunkAt(x, z).setForceLoaded(true);
            }
        }
    }
}
