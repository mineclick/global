package net.mineclick.global.util;

import com.google.common.collect.ImmutableMap;
import lombok.Setter;
import net.mineclick.global.config.SaveConstructor;
import net.mineclick.global.model.PlayerModel;
import org.bukkit.ChatColor;

import java.io.Serial;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.Map;

@Setter
@SaveConstructor(args = "value")
public class BigNumber extends BigDecimal {
    public static final BigNumber ZERO = new BigNumber(0);
    public static final BigNumber ONE = new BigNumber(1);
    public static final int POW_MAX = 9999;
    @Serial
    private static final long serialVersionUID = 5640091003483373482L;
    private static final DecimalFormat formatter = new DecimalFormat("000.00");
    private static final DecimalFormat formatterRounded = new DecimalFormat("000");
    private static final DecimalFormat formatterNoZeroes = new DecimalFormat("###.##");
    private static final DecimalFormat formatterNoZeroesRounded = new DecimalFormat("###");

    private static final Map<Integer, String> abbreviations = ImmutableMap.<Integer, String>builder()
            .put(0, "")
            .put(3, "K")
            .put(6, "M")
            .put(9, "B")
            .put(12, "T")
            .put(15, "Q")
            .put(18, "Qn")
            .put(21, "S")
            .put(24, "Sp")
            .put(27, "Oc")
            .put(30, "No")
            .put(33, "Dc")
            .put(36, "Ud")
            .put(39, "Dd")
            .put(42, "Td")
            .put(45, "Qad")
            .put(48, "Qid")
            .put(51, "Sxd")
            .put(54, "Spd")
            .put(57, "Ocd")
            .put(60, "Nod")
            .put(63, "Vg")
            .put(66, "Uvg")
            .put(69, "Dvg")
            .put(72, "Tvg")
            .put(75, "Qavg")
            .put(78, "Qivg")
            .put(81, "Sxvg")
            .put(84, "Spvg")
            .put(87, "Ocvg")
            .build();

    private final String value = toString(); // Do not delete. Needed for the SaveConstructor
    private boolean roundOff = false;
    private boolean noZeroes = true;
    private ChatColor mainColor = ChatColor.YELLOW;
    private ChatColor zeroesColor = ChatColor.DARK_GRAY;
    private ChatColor abbreviationColor = ChatColor.GOLD;

    public BigNumber(String val) {
        super(val, MathContext.DECIMAL32);
    }

    public BigNumber(double val) {
        super(val, MathContext.DECIMAL32);
    }

    public BigNumber(BigDecimal decimal) {
        this(decimal.toString());
    }

    public BigNumber add(BigNumber number) {
        return new BigNumber(super.add(number, MathContext.DECIMAL32));
    }

    public BigNumber subtract(BigNumber number) {
        return new BigNumber(super.subtract(number, MathContext.DECIMAL32));
    }

    public BigNumber multiply(BigNumber number) {
        return new BigNumber(super.multiply(number, MathContext.DECIMAL32));
    }

    public BigNumber divide(BigNumber number) {
        return new BigNumber(super.divide(number, MathContext.DECIMAL32));
    }

    public BigNumber pow(long number) {
        return pow((int) number);
    }

    @Override
    public BigNumber pow(int number) {
        return new BigNumber(super.pow(number, MathContext.DECIMAL32));
    }

    public String print(PlayerModel player) {
        return print(player, roundOff, noZeroes);
    }

    public String print(PlayerModel player, boolean roundOff, boolean noZeroes) {
        return print(player, roundOff, noZeroes, false);
    }

    public String print(PlayerModel player, boolean roundOff, boolean noZeroes, boolean bold) {
        return print(player, roundOff, noZeroes, bold ? ChatColor.AQUA : mainColor, zeroesColor, abbreviationColor, bold);
    }

    public String print(PlayerModel player, boolean roundOff, boolean noZeroes, ChatColor mainColor, ChatColor zeroesColor, ChatColor abbreviationColor, boolean bold) {
        String num = super.toEngineeringString();

        if (num.startsWith("-"))
            return "0";

        int exponent = 0;
        if (num.contains("E+")) {
            try {
                exponent = Integer.parseInt(num.substring(num.indexOf("E+") + 2));
                num = num.substring(0, num.indexOf("E+"));
            } catch (NumberFormatException ignored) {
            }
        }

        if (exponent == 0) {
            double value = 0;
            try {
                value = Double.parseDouble(num);
            } catch (NumberFormatException ignored) {
            }

            if (value >= 1000000) {
                exponent = 6;
                num = String.valueOf(value / 1000000D);
            } else if (value >= 1000) {
                exponent = 3;
                num = String.valueOf(value / 1000D);
            }
        }

        try {
            DecimalFormat format = noZeroes ? (roundOff ? formatterNoZeroesRounded : formatterNoZeroes) : (roundOff ? formatterRounded : formatter);
            num = format.format(Double.parseDouble(num));
        } catch (NumberFormatException ignored) {
        }

        int zeroIndex = 0;
        int dotIndex = num.indexOf(".");
        while (zeroIndex < num.length() && num.charAt(zeroIndex) != '.' && Integer.parseInt(String.valueOf(num.charAt(zeroIndex))) == 0)
            zeroIndex++;

        String b = (bold ? ChatColor.BOLD.toString() : "");
        num = (zeroIndex > 0 ? zeroesColor : "") + b
                + num.substring(0, dotIndex == zeroIndex ? zeroIndex - 1 : zeroIndex)
                + mainColor + b
                + num.substring(dotIndex == zeroIndex ? zeroIndex - 1 : zeroIndex);

        if (exponent == 0) {
            return num;
        }
        String abbreviation = "E" + exponent;
        if (player != null && !player.getGameSettings().getNumericNotation().get() && abbreviations.containsKey(exponent)) {
            abbreviation = abbreviations.get(exponent);
        }
        return num + abbreviationColor + b + abbreviation;
    }

    public boolean greaterThanOrEqual(BigNumber other) {
        return compareTo(other) >= 0;
    }

    public boolean greaterThan(BigNumber other) {
        return compareTo(other) > 0;
    }

    public boolean smallerThanOrEqual(BigNumber other) {
        return compareTo(other) <= 0;
    }

    public boolean smallerThan(BigNumber other) {
        return compareTo(other) < 0;
    }

    public boolean equals(BigNumber other) {
        return compareTo(other) == 0;
    }

    @Override
    public String toString() {
        return toEngineeringString();
    }

    public int getExponent() {
        return precision() - scale() - 1;
    }
}
