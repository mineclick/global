package net.mineclick.global.util;


import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Base64;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Skins {
    private static final LoadingCache<String, PlayerProfile> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(30, TimeUnit.MINUTES)
            .build(new CacheLoader<>() {
                @Override
                public PlayerProfile load(String base64) {
                    return createFakeProfile(base64);
                }
            });

    public static String getUsernameFromTexture(String texture) {
        String decoded = new String(Base64.getDecoder().decode(texture)).replace(" ", "");
        String lookup = "profileName\":\"";
        if (decoded.contains(lookup)) {
            int index = decoded.indexOf(lookup) + lookup.length();
            String name = decoded.substring(index);
            name = name.substring(0, name.indexOf("\""));
            return name;
        }

        return null;
    }

    public static String loadSkin(String base64) {
        cache.getUnchecked(base64);
        return base64;
    }

    public static PlayerProfile createFakeProfile(String base64) {
        PlayerProfile profile = Bukkit.createProfile(UUID.randomUUID(), RandomStringUtils.random(10));
        profile.getProperties().add(new ProfileProperty("textures", base64));

        return profile;
    }

    public static ItemStack set(ItemStack stack, String skin) {
        if (stack.getType().equals(Material.PLAYER_HEAD)) {
            SkullMeta meta = (SkullMeta) stack.getItemMeta();
            meta.setPlayerProfile(cache.getUnchecked(skin));

            stack.setItemMeta(meta);
        }

        return stack;
    }
}
