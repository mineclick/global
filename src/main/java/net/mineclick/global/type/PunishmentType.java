package net.mineclick.global.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.mineclick.global.model.OffenceData;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.util.Formatter;
import net.mineclick.global.util.MessageType;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.time.temporal.ChronoUnit;
import java.util.function.BiConsumer;

@Getter
@RequiredArgsConstructor
public enum PunishmentType {
    WARNING(Rank.STAFF, (player, offenceData) -> {
        player.sendMessage("You have been reported for " + offenceData.getType().getReason(), MessageType.ERROR);
        player.sendMessage("Consequences will take place", MessageType.ERROR);
    }, new int[]{0}),
    MUTE(Rank.STAFF, (player, offenceData) -> {
        player.sendMessage("You have been muted for "
                + offenceData.getDurationMinutes()
                + " minutes", MessageType.ERROR);
    }, new int[]{15, 30, 60, 1440}),
    KICK(Rank.STAFF, (player, offenceData) -> {
        Player bukkitPlayer = player.getPlayer();
        if (bukkitPlayer != null) {
            bukkitPlayer.kickPlayer(ChatColor.RED
                    + "You were kicked for " + offenceData.getType().getReason()
            );
        }
    }, new int[]{0}),
    BAN(Rank.STAFF, (player, offenceData) -> {
        String duration = offenceData.isPermanent() ? "permanent"
                : Formatter.duration(offenceData.getPunishedOn().plus(offenceData.getDurationMinutes(), ChronoUnit.MINUTES).toEpochMilli() - System.currentTimeMillis());

        String msg = ChatColor.RED + "You are banned from MineClick\n\n"
                + ChatColor.GOLD + "Reason: " + ChatColor.GRAY + offenceData.getType().getReason() + "\n"
                + ChatColor.GOLD + "Duration: " + ChatColor.GRAY + duration + "\n\n"
                + ChatColor.GRAY + "Appeal at " + ChatColor.DARK_AQUA + "ender.mineclick.net/appeal";

        Player bukkitPlayer = player.getPlayer();
        if (bukkitPlayer != null) {
            bukkitPlayer.kickPlayer(msg);
        }
    }, new int[]{30, 120, 270, 1440, 2880, -1});

    private final Rank rankRequirement;
    private final BiConsumer<PlayerModel, OffenceData> apply;
    private final int[] durations;
}
