package net.mineclick.global.config;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.global.config.field.*;
import net.mineclick.global.config.field.specials.ConveyorBelt;
import net.mineclick.global.util.BigNumber;
import net.mineclick.global.util.location.ConfigLocation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@Getter
@Setter
public class IslandConfig extends ConfigFile {
    private final List<BuildingConfig> buildings = new ArrayList<>();
    private DimensionConfig dimension;

    @Save
    private BigNumber baseCost;
    @Save
    private double multiplier;
    @Save
    private ArrayList<ConfigLocation> npcSpawns;
    @Save
    private ArrayList<MineRegionConfig> mineRegions;
    @Save
    private ConfigLocation spawn;
    @Save
    private ConfigLocation teleporter;
    @Save
    private ConfigLocation tutorialVillagerSpawn;
    @Save
    private ParkourConfig parkour;
    @Save
    private ArrayList<UpgradeConfig> parkourUpgrades;
    @Save
    private ArrayList<IslandUnlockRequired> unlockRequired;
    @Save
    private boolean nightTime;
    @Save
    private ArrayList<MineBlock> globalMineBlocks;

    //Specials
    @Save
    private ArrayList<ConveyorBelt> conveyorBelts;

    public IslandConfig(File dir) {
        super(dir);

        buildings.addAll(loadAll(dir, BuildingConfig.class));
        buildings.forEach(b -> b.setIsland(this));
    }

    public List<MineRegionConfig> getAllMineRegions() {
        return getAllMineRegions(b -> true);
    }

    public List<MineRegionConfig> getAllMineRegions(Predicate<BuildingConfig> filter) {
        List<MineRegionConfig> list = new ArrayList<>(mineRegions);
        buildings.stream().filter(filter).forEach(b -> list.addAll(b.getExtraMineRegions()));

        return list;
    }
}
