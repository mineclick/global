package net.mineclick.global.config.field;

import lombok.Getter;
import net.mineclick.global.config.SaveConstructor;
import org.bukkit.Material;

@Getter
@SaveConstructor(args = {"block", "item"})
public class MineBlock {
    private final String block;
    private final String item;

    private final Material blockMaterial;
    private final Material itemMaterial;

    public MineBlock(String block, String item) {
        this.block = block;
        this.item = item;

        blockMaterial = Material.getMaterial(block);
        itemMaterial = Material.getMaterial(item);
    }

    public MineBlock(Material blockMaterial, Material itemMaterial) {
        this.blockMaterial = blockMaterial;
        this.itemMaterial = itemMaterial;

        this.block = blockMaterial.name();
        this.item = itemMaterial.name();
    }

    @Override
    public String toString() {
        return blockMaterial.toString();
    }
}
