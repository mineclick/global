package net.mineclick.global.config.field;

import lombok.Getter;
import net.mineclick.global.config.SaveConstructor;
import net.mineclick.global.model.PlayerModel;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

@Getter
@SaveConstructor(args = {"type", "arg"})
public class UpgradeConfig {
    private final String type;
    private final String arg;

    private final UpgradeType upgradeType;

    public UpgradeConfig(String type, String arg) {
        this.type = type;
        this.arg = arg;

        upgradeType = UpgradeType.valueOf(type);
    }

    public UpgradeConfig(UpgradeType type, String arg) {
        this.type = type.toString();
        this.arg = arg;

        upgradeType = type;
    }

    public void apply(PlayerModel player) {
        upgradeType.getConsumer().accept(player, arg);
    }

    public String checkPrerequisite(PlayerModel player) {
        return upgradeType.getPrerequisite().apply(player, arg);
    }

    @Override
    public String toString() {
        return type + ":" + arg;
    }

    public String getDescription(PlayerModel playerModel) {
        if (upgradeType.descriptor == null || (upgradeType.argCheck != null && !upgradeType.argCheck.apply(arg)))
            return "???";
        return upgradeType.descriptor.apply(playerModel, arg);
    }

    @Getter
    public enum UpgradeType {
        WORKER_BPS,
        PICKAXE_AMOUNT,
        GOLD,
        SUPER_BLOCK;

        private BiConsumer<PlayerModel, String> consumer;
        private BiFunction<PlayerModel, String, String> prerequisite = (playerData, s) -> null;
        private Function<String, Boolean> argCheck = s -> true;
        private BiFunction<PlayerModel, String, String> descriptor = (p, s) -> this.name() + " -> " + s;

        public UpgradeType setConsumer(BiConsumer<PlayerModel, String> consumer) {
            this.consumer = consumer;
            return this;
        }

        public UpgradeType setPrerequisite(BiFunction<PlayerModel, String, String> prerequisite) {
            this.prerequisite = prerequisite;
            return this;
        }

        public UpgradeType setArgCheck(Function<String, Boolean> argCheck) {
            this.argCheck = argCheck;
            return this;
        }

        public UpgradeType setDescriptor(BiFunction<PlayerModel, String, String> descriptor) {
            this.descriptor = descriptor;
            return this;
        }
    }
}
